//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, TextInput, TouchableOpacity } from 'react-native';
import { RadioButton, List, Switch } from 'react-native-paper';
import CheckBox from '@react-native-community/checkbox';

// create a component
const Display = ({navigation}) => {
    const [time, setTime] = React.useState('timeFirst');
    const [date, setDate] = React.useState('dateFirst');
    const [flight, setFlight] = React.useState('flightFirst');
    const [roleChecked, setRoleChecked] = React.useState('first');
    const [instructor, setInstructor] = React.useState(false);
    const [cross, setCross] = React.useState(false);
    const [instrument, setInstrument] = React.useState(false);
    const [approach, setApproach] = React.useState(false);
    const [number, onChangeNumber] = React.useState('15');
    
    return (
        <ScrollView>
        <View style={styles.container}>
            {/* Time format */}

            <Text style={styles.text}>Time Format</Text>
                <View style={styles.fieldContainer}>
                    <RadioButton
                        value="timeFirst"
                        status={ time === 'timeFirst' ? 'checked' : 'unchecked' }
                        onPress={() => setTime('timeFirst')}
                    />
                    <Text style={styles.fieldText}>HH:MM</Text>
                </View>
            {/* Date format */}

            <Text style={styles.text}>Date Format</Text>
                <View style={styles.fieldContainer}>
                    <RadioButton
                        value="dateFirst"
                        status={ date === 'dateFirst' ? 'checked' : 'unchecked' }
                        onPress={() => setDate('dateFirst')}
                    />
                    <Text style={styles.fieldText}>DD MM YYYY</Text>
                    <View style={{marginLeft: 20,}}>
                    <RadioButton
                        value="dateSecond"
                        status={ date === 'dateSecond' ? 'checked' : 'unchecked' }
                        onPress={() => setDate('dateSecond')}
                    />
                    </View>
                    <Text style={styles.fieldText}>MM DD YYYY</Text>
                </View>
            {/* flight Time Log */}

            <Text style={styles.text}>Flight Time Log</Text>
                <View style={styles.fieldContainer}>
                    <RadioButton
                        value="flightFirst"
                        status={ flight === 'flightFirst' ? 'checked' : 'unchecked' }
                        onPress={() => setFlight('dateFirst')}
                    />
                    <Text style={styles.fieldText}>Chocks off/{'\n'}Chocks On</Text>
                </View>
                {/* My Aircraft */}

                <Text style={styles.text}>My Aircraft</Text>
                  <TouchableOpacity onPress={()=> navigation.navigate('Aircraft')}>
                    <View style= {styles.default}>
                        <Text>Type</Text>
                        <Text style={{marginLeft: 250,}}>Aircraft Type</Text>
                    </View>
                 </TouchableOpacity>
                    <View style= {styles.default}>
                        <Text>Aircraft ID</Text>
                        <TextInput 
                        placeholder='Aircraft ID'
                        style={{marginLeft: 220, marginTop: -10,}}/>
                    </View>
                {/* Role */}

                <Text style={styles.text}>Role</Text>
                  <View style= {styles.roleView}>
                  <RadioButton
                        value="first"
                        status={ roleChecked === 'first' ? 'checked' : 'unchecked' }
                        onPress={() => setRoleChecked('first')}
                    />
                    <Text style={styles.fieldText}>Airline Captain</Text>
                  <RadioButton
                        value="second"
                        status={ roleChecked === 'second' ? 'checked' : 'unchecked' }
                        onPress={() => setRoleChecked('second')}
                    />
                    <Text style={styles.fieldText}>Airline First Officer</Text>
                    </View>
                    <View style= {styles.roleView}>
                    <RadioButton
                            value="third"
                            status={ roleChecked === 'third' ? 'checked' : 'unchecked' }
                            onPress={() => setRoleChecked('third')}
                        />
                        <Text style={styles.fieldText}>Airline Instructor</Text>
                    <RadioButton
                            value="fourth"
                            status={ roleChecked === 'fourth' ? 'checked' : 'unchecked' }
                            onPress={() => setRoleChecked('fourth')}
                        />   
                        <Text style={styles.fieldText}>Flight Cadet</Text>
                  </View>
                  <View style= {styles.roleView}>
                    <Text>Academy Instructor</Text>
                    <CheckBox
                        value={instructor}
                        onValueChange={setInstructor}
                        style={{marginLeft: 40}}
                        />
                  </View>
                  <View style= {styles.roleView}>
                    <Text>Cross Country</Text>
                    <CheckBox
                        value={cross}
                        onValueChange={setCross}
                        style={{marginLeft: 75}}
                        />
                  </View>
                  <View style= {styles.roleView}>
                    <Text>Actual Instrument</Text>
                    <CheckBox
                        value={instrument}
                        onValueChange={setInstrument}
                        style={{marginLeft: 55}}
                        />
                  </View>
                  <Text>{instrument ? 
                  <View style= {styles.roleView}>
                    <Text style={{fontSize: 25,}}>Block Time - </Text>
                    <TextInput onChangeText={onChangeNumber}
                    value={number}
                    placeholder="Time"
                    keyboardType="numeric"
                    style={{marginLeft:160, marginTop:-10,}}/>
                  </View> : ''}
                  </Text>

                  <View style= {styles.roleView}>
                    <Text>Approach 1</Text>
                    <CheckBox
                        value={approach}
                        onValueChange={setApproach}
                        style={{marginLeft: 95}}
                        />
                  </View>
                {/* Mode */}   

            <Text style={styles.text}>Mode</Text>
            <View  style={styles.default}>
                <Text style={styles.fieldText}>Change to Night Mode</Text>
                <List.Item
                title="Dark Mode"
                // left={() => <List.Icon icon="brightness-4" />}
                right={() => <Switch />}
                style={{marginLeft: 80,}}
            />
            </View>
        </View>
        </ScrollView>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        //backgroundColor: '#2c3e50',
    },
    text: {
        color: 'grey',
        fontSize: 15,
        marginTop: 15,
        marginLeft: 15,
        fontWeight: 'bold',
    },
    fieldContainer:{
        backgroundColor: '#fff',
        padding: 10,
        width: '100%',
        marginTop: 5,
        flexDirection: 'row',
        //justifyContent: 'space-evenly',
    },
    fieldText: {
        fontSize: 18,
        marginTop: 5,
        },
    default: {
      backgroundColor:'#fff',
      padding: 10,
      width: '100%',
      borderBottomColor:'grey',
      borderBottomWidth: 1,
      flexDirection: 'row',
    },
    roleView: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        padding: 10,
        width: '100%',
        borderBottomColor:'grey',
        borderBottomWidth: 1,
    },
});

//make this component available to the app
export default Display;
