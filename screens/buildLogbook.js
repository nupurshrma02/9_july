//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, ScrollView, TouchableOpacity } from 'react-native';
import { RadioButton, } from 'react-native-paper';
import { color } from 'react-native-reanimated';

// create a component
const BuildLogbook = () => {
    const [category, setCategory] = React.useState('cFirst');
    const [engine, setEngine] = React.useState('eFirst');
    const [Class, setClass] = React.useState('ClassFirst');

    return (
        <ScrollView>
        <View style={styles.container}>
            {/*Second aircraft */}
            
            <Text style={styles.text}>Second Aircraft</Text>
            <View style={styles.field}>
              <Text style={styles.fieldText}>Aircraft Type</Text>
              <TextInput placeholder='Aircraft type' style={{marginTop:-10, marginLeft:180,}}/>
            </View>
            {/*Category*/}
         
            <Text style={styles.text}>Category</Text>
            <View style={styles.field}>
                  <RadioButton
                        value="cFirst"
                        status={ category === 'cFirst' ? 'checked' : 'unchecked' }
                        onPress={() => setCategory('cFirst')}
                    />
                    <Text style={styles.fieldText}>Air plane</Text>
                  <RadioButton
                        value="cSecond"
                        status={ category === 'cSecond' ? 'checked' : 'unchecked' }
                        onPress={() => setCategory('cSecond')}
                    />
                    <Text style={styles.fieldText}>Microlight</Text>
            </View>
            <View style={styles.field}>
                  <RadioButton
                        value="cThird"
                        status={ category === 'cThird' ? 'checked' : 'unchecked' }
                        onPress={() => setCategory('cThird')}
                    />
                    <Text style={styles.fieldText}>Helicopter</Text>
                  <RadioButton
                        value="cFourth"
                        status={ category === 'cFourth' ? 'checked' : 'unchecked' }
                        onPress={() => setCategory('cFourth')}
                    />
                    <Text style={styles.fieldText}>Glider</Text>
            </View>
            {/*Engine*/}

            <Text style={styles.text}>Engine</Text>
            <View style={styles.field}>
                  <RadioButton
                        value="eFirst"
                        status={ engine === 'eFirst' ? 'checked' : 'unchecked' }
                        onPress={() => setEngine('eFirst')}
                    />
                    <Text style={styles.fieldText}>Jet</Text>
                  <RadioButton
                        value="eSecond"
                        status={ engine === 'eSecond' ? 'checked' : 'unchecked' }
                        onPress={() => setEngine('eSecond')}
                    />
                    <Text style={styles.fieldText}>Turbo Prop</Text>
                  <RadioButton
                        value="eThird"
                        status={ engine === 'eThird' ? 'checked' : 'unchecked' }
                        onPress={() => setEngine('eThird')}
                    />
                    <Text style={styles.fieldText}>Turbo-Shaft</Text>
            </View>
            <View style={styles.field}>
                  <RadioButton
                        value="eFourth"
                        status={ engine === 'eFourth' ? 'checked' : 'unchecked' }
                        onPress={() => setEngine('eFourth')}
                    />
                    <Text style={styles.fieldText}>Piston</Text>
                  <RadioButton
                        value="eFifth"
                        status={ engine === 'eFifth' ? 'checked' : 'unchecked' }
                        onPress={() => setEngine('eFifth')}
                    />
                    <Text style={styles.fieldText}>Not Powered</Text>
            </View>
            
            <View style={styles.field}>
            <Text style={styles.fieldText}>Engine Name</Text>
            <TextInput placeholder='Engine Name' style={{marginTop:-10, marginLeft:180,}}/>
            </View>
            
            <View style={styles.field}>
            <Text style={styles.fieldText}>Class</Text>
                    <RadioButton
                            value="ClassFirst"
                            status={ Class === 'ClassFirst' ? 'checked' : 'unchecked' }
                            onPress={() => setClass('ClassFirst')}
                        />
                        <Text style={styles.fieldText}>ME Land</Text>
                    <RadioButton
                            value="ClassSecond"
                            status={ Class === 'ClassSecond' ? 'checked' : 'unchecked' }
                            onPress={() => setClass('ClassSecond')}
                        />
                        <Text style={styles.fieldText}>ME Sea</Text> 
            </View>
            <View style={styles.field}>
                    <RadioButton
                            value="ClassThird"
                            status={ Class === 'ClassThird' ? 'checked' : 'unchecked' }
                            onPress={() => setClass('ClassThird')}
                        />
                        <Text style={styles.fieldText}>SE Land</Text>
                    <RadioButton
                            value="ClassFourth"
                            status={ Class === 'ClassFourth' ? 'checked' : 'unchecked' }
                            onPress={() => setClass('ClassFourth')}
                        />
                        <Text style={styles.fieldText}>SE Sea</Text> 
            </View>
            {/*Time*/}

            <Text style={{...styles.text, ...styles.unImpFields}}>Time</Text>
            
            {/*Day*/}
            <View style={styles.field}>
                <Text style={{fontSize:15, marginLeft:10}}>Day</Text>
            </View>
            <View style={styles.field}>
            <Text style={{marginLeft:20,}}>PIC/P1</Text>
            <TextInput placeholder='HH:MM' style={{marginTop:-10, marginLeft:200,}}/>
            </View>
            <View style={styles.field}>
            <Text style={{marginLeft:20,}}>SIC/P2</Text>
            <TextInput placeholder='HH:MM' style={{marginTop:-10, marginLeft:200,}}/>
            </View>
            <View style={styles.field}>
            <Text style={{marginLeft:20,}}>P1(U/S)</Text>
            <Text style={{marginLeft:200, color:'grey'}}>N/A</Text>
            </View>
            <View style={styles.field}>
            <Text style={{marginLeft:20,}}>P1(U/T)</Text>
            <Text style={{marginLeft:200, color:'grey'}}>N/A</Text>
            </View>
            <View style={styles.field}>
            <Text style={{marginLeft:20,}}>Total</Text>
            <TextInput placeholder='HH:MM' style={{marginTop:-10, marginLeft:200,}}/>
            </View>

            {/*Night*/}
            <View style={styles.field}>
                <Text style={{fontSize:15, marginLeft:10}}>Night</Text>
            </View>
            <View style={styles.field}>
            <Text style={{marginLeft:20,}}>PIC/P1</Text>
            <TextInput placeholder='HH:MM' style={{marginTop:-10, marginLeft:200,}}/>
            </View>
            <View style={styles.field}>
            <Text style={{marginLeft:20,}}>SIC/P2</Text>
            <TextInput placeholder='HH:MM' style={{marginTop:-10, marginLeft:200,}}/>
            </View>
            <View style={styles.field}>
            <Text style={{marginLeft:20,}}>P1(U/S)</Text>
            <Text style={{marginLeft:200, color:'grey'}}>N/A</Text>
            </View>
            <View style={styles.field}>
            <Text style={{marginLeft:20,}}>P1(U/T)</Text>
            <Text style={{marginLeft:200, color:'grey'}}>N/A</Text>
            </View>
            <View style={styles.field}>
            <Text style={{marginLeft:20,}}>Total</Text>
            <TextInput placeholder='HH:MM' style={{marginTop:-10, marginLeft:200,}}/>
            </View>
            <View style={styles.field}>
                <Text style={{fontSize:15, marginLeft:10}}>Total Flying Time</Text>
                <TextInput placeholder='HH:MM' style={{marginTop:-10, marginLeft:150,}}/>
            </View>
            {/*Instrumental time*/}

            <View style={styles.field}>
                <Text style={{fontSize:15, marginLeft:10}}>Instrumental Time</Text>
            </View>
            <View style={styles.field}>
            <Text style={{marginLeft:20,}}>Actual</Text>
            <TextInput placeholder='HH:MM' style={{marginTop:-10, marginLeft:200,}}/>
            </View>
            <View style={styles.field}>
            <Text style={{marginLeft:20,}}>Simulated</Text>
            <TextInput placeholder='HH:MM' style={{marginTop:-10, marginLeft:200,}}/>
            </View>
            <View style={styles.field}>
            <Text style={{marginLeft:10,}}>Simulator</Text>
            <TextInput placeholder='HH:MM' style={{marginTop:-10, marginLeft:200,}}/>
            </View>
            {/*Instructional Flying*/}

            <View style={styles.field}>
                <Text style={{fontSize:15, marginLeft:10}}>Instructional Flying</Text>
            </View>
            <View style={styles.field}>
            <Text style={{marginLeft:20,}}>Day</Text>
            <TextInput placeholder='HH:MM' style={{marginTop:-10, marginLeft:200,}}/>
            </View>
            <View style={styles.field}>
            <Text style={{marginLeft:20,}}>Night</Text>
            <TextInput placeholder='HH:MM' style={{marginTop:-10, marginLeft:200,}}/>
            </View>
            <View style={styles.field}>
            <Text style={{marginLeft:20,}}>Total</Text>
            <TextInput placeholder='HH:MM' style={{marginTop:-10, marginLeft:200,}}/>
            </View>
            {/*T/O & Landing*/}

            <Text style={{...styles.text, ...styles.unImpFields}}>T/O & Landing</Text>
            <View style={styles.field}>
            <Text style={{marginLeft:20,}}>Day T/O</Text>
            <TextInput placeholder='Please Enter Number' style={{marginTop:-10, marginLeft:150,}}/>
            </View>
            <View style={styles.field}>
            <Text style={{marginLeft:20,}}>Night T/O</Text>
            <TextInput placeholder='Please Enter Number' style={{marginTop:-10, marginLeft:150,}}/>
            </View>
            <View style={styles.field}>
            <Text style={{marginLeft:20,}}>Day Landing</Text>
            <TextInput placeholder='Please Enter Number' style={{marginTop:-10, marginLeft:130,}}/>
            </View>
            <View style={styles.field}>
            <Text style={{marginLeft:20,}}>Night Landing</Text>
            <TextInput placeholder='Please Enter Number' style={{marginTop:-10, marginLeft:120,}}/>
            </View>
            {/*Remark*/}

            <Text style={{...styles.text, ...styles.unImpFields}}>Remark</Text>
            <View style={{padding:40, backgroundColor:'#fff',marginBottom:10,}}>
                <View style={styles.remarksBox}>
                    <TextInput placeholder='Your Remarks...'/>
                </View>
            </View>
            {/*Buttons*/}
           
            <View style={{flexDirection:'row',justifyContent:'space-between', marginBottom:10,}}>
            <TouchableOpacity>
                <View style={styles.button}>
                <Text style={styles.buttonText} >Edit</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity>
                <View style={styles.button}>
                <Text style={styles.buttonText} >Save & Add another Aircraft</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity>
                <View style={styles.button}>
                <Text style={styles.buttonText} >Upload Logbook</Text>
                </View>
            </TouchableOpacity>
            </View>

        </View>
        </ScrollView>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        //alignItems: 'center',
        //backgroundColor: '#2c3e50',
    },
    text:{
        fontSize: 20,
        marginTop: 10,
        marginLeft:10,
    },
    field:{
      backgroundColor: '#fff',
      padding:5,
      marginTop: 5,
      flexDirection: 'row',
    },
    fieldText: {
        fontSize: 18,
        marginTop: 5,
        },
    unImpFields: {
      color: 'grey',
    },
    remarksBox:{
        borderWidth:1,
        borderColor:'#000',
        borderRadius:10,
        paddingRight: 10,
        paddingVertical:10,
    },
    button: {
        backgroundColor:'red',
        padding: 10,
        marginTop: 10,
        width: 100,
        borderRadius:10,
        alignItems:'center'
    },
    buttonText:{
      fontWeight: 'bold',
      color: '#fff',
    },

});

//make this component available to the app
export default BuildLogbook;
